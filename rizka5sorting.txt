
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Arrays;

public class rizka5sorting {

	
	    public static void main (String[] args){
	        boolean ulang = false;
	        BufferedReader brInput = new BufferedReader(new InputStreamReader(System.in));
	        System.out.println("-- Program Mengurutkan Barang --");
	        mainMenu();
	        do{
	            try{
	                try{
	                    System.out.print("Pilih Menu ==>");
	                    String choiceString = brInput.readLine();
	                    byte choice = Byte.parseByte(choiceString);
	                    switch(choice){
	                        case 1:
	                            boolean ulang2 = false;
	                            do{
	                                try{
	                                    System.out.print("Masukkan Jumlah Barang : ");
	                                    String sumString = brInput.readLine();
	                                    System.out.println();
	                                    byte sum = Byte.parseByte(sumString);
	                                    if(sum<0){
	                                        System.out.println("Jumlah Barang Tidak Boleh Minus");
	                                        ulang2 = true;
	                                        continue;
	                                    }
	                                    ulang = false;
	                                    ulang2 = false;
	                                    inputItem(sum);
	                                }
	                                catch(NumberFormatException e){
	                                    System.out.println("Masukkan Angka !");
	                                    ulang2 = true;
	                                }
	                            }while(ulang2);
	                            break;
	                        case 2:
	                            subMenuShorting();
	                            System.out.print("Pilih Menu ==>");
	                            String choice2String = brInput.readLine();
	                            byte choice2 = Byte.parseByte(choice2String);
	                            switch(choice2){
	                                case 1 :
	                                    readAndSort();
	                                    break;
	                                    
	                            }
	                            break;
	                        default :
	                            System.out.println("Pilih Hanya Angka 1 atau 2 !");
	                            ulang = true;
	                            continue;
	                    }
	                }
	                catch(NumberFormatException e){
	                    System.out.println("Masukkan Angka !");
	                    ulang = true;
	                }
	            }
	            catch(IOException e){
	            System.out.println("Inputan Salah !");
	            ulang = true;
	            }
	        }
	        while(ulang);
	    }

	    public static void mainMenu(){
	        System.out.println("Main Menu");
	        System.out.println("1. Input Barang");
	        System.out.println("2. Urutkan Barang");
	    }

	    public static void subMenuShorting(){
	        System.out.println("Urutkan Barang Berdasarkan");
	        System.out.println("1. Kode Barang");
	        System.out.println("2. Nama Barang");
	        System.out.println("3. Warna Barang");
	        System.out.println("4. Jumlah Barang");
	        System.out.println("5. Harga Barang");
	    }

	    public static void inputItem(int sum){
	        boolean ulang = false;
	        BufferedReader brInput = new BufferedReader(new InputStreamReader(System.in));
	        do{
	            try{
	                String sourceFile = "sourceFile.txt";
	                File fileSource = new File(sourceFile);
	                FileWriter fileWriterSource = new FileWriter(fileSource.getAbsoluteFile());
	                BufferedWriter bwSourceFile = new BufferedWriter(fileWriterSource);

	                byte[] item = new byte[sum];
	                String[] character = new String[5];
	                byte indeksItem=0; 
	                while(indeksItem<sum){
	                    if(!fileSource.exists()){
	                        fileSource.createNewFile();
	                    }
	                    try{
	                        System.out.println("Masukkan Barang ke-"+(indeksItem+1)+" ==> ");
	                        System.out.print("Kode Barang (hanya tiga digit, contoh 003) : ");
	                        character[0] = brInput.readLine();
	                        int code = Integer.parseInt(character[0]);
	                        if(code>999){
	                            System.out.println("Masukkan hanya tiga digit angka !");
	                            continue;
	                        }else if(code<0){
	                            System.out.println("Kode Tidak Boleh Minus !");
	                            continue;
	                        }else if(code>=0 && code<10){
	                            character[0] = "00"+code;
	                        }else if(code>=10 && code<100){
	                            character[0] = "0"+code;
	                        }
	                        System.out.print("Nama Barang : ");
	                        character[1] = brInput.readLine();
	                        System.out.print("Warna Barang : ");
	                        character[2] = brInput.readLine();
	                        System.out.print("Jumlah Barang : ");
	                        character[3] = brInput.readLine();
	                        int quantity = Integer.parseInt(character[3]);
	                        if(quantity<=0){
	                            System.out.println("Jumlah Barang Tidak Boleh Nol atau Minus !");
	                            continue;
	                        }
	                        System.out.print("Harga Barang : ");
	                        character[4] = brInput.readLine();
	                        int price = Integer.parseInt(character[4]);
	                        if(price<=0){
	                            System.out.println("Harga Barang Tidak Boleh Nol atau Minus !");
	                            continue;
	                        }
	                        System.out.println();

	                        byte indeksCharacter = 0;
	                        while(indeksCharacter<5){
	                            if(indeksCharacter==0){
	                                bwSourceFile.write("=====");
	                                bwSourceFile.newLine();
	                            }
	                            bwSourceFile.write(character[indeksCharacter]);
	                            bwSourceFile.newLine();

	                            indeksCharacter++;
	                        }

	                        indeksItem++;
	                    }catch(NumberFormatException e){
	                        System.out.println("Masukkan Angka !");
	                    }
	                }
	                bwSourceFile.close();
	                ulang = false;
	            }
	            catch(NegativeArraySizeException e){
	                System.out.println("Inputan Tidak Boleh Minus !");
	                ulang = true;
	            }
	            catch(IOException e){
	                System.out.println("Inputan Salah !");
	                ulang = true;
	            }
	        }while(ulang);
	    }

	    public static void readAndSort(){
	        boolean ulang = false;
	        do{
	            String sourceFile = "sourceFile.txt";
	            try{
	                BufferedReader brCekSourceFile = new BufferedReader(new FileReader(new File(sourceFile)));
	                String contentCheck = null;
	                byte sumIndeksItem = 0;
	                while((contentCheck = brCekSourceFile.readLine()) != null){
	                    if(contentCheck.equals("=====")){
	                        sumIndeksItem++;
	                    }
	                }
	                
	                BufferedReader brSourceFile = new BufferedReader(new FileReader(new File(sourceFile)));
	                String content = null;
	                
	                String[][] character = new String[sumIndeksItem][5];
	                String temp;
	                
	                byte indeksItem = -1;
	                byte indeksCharacter = 0;
	                while((content = brSourceFile.readLine()) != null){
	                    if(content.equals("=====")){
	                        indeksCharacter = 0;
	                        indeksItem++;
	                        continue;
	                    }
	                    character[indeksItem][indeksCharacter] = content;
	                    indeksCharacter++;
	                }
	                
	                String[] newCharacter = new String[5];
	                String tukar;
	                for(int i=0; i<sumIndeksItem; i++){
	                    for (int j=0; j<5; j++){
	                        newCharacter[j] = character[i][j];
	                    }
	                }
	                for(int a=0; a<5; a++){
	                    for(int b=a+1; b<5; b++){
	                        if(newCharacter[a].compareTo(newCharacter[b])>0){
	                            tukar = newCharacter[a];
	                            newCharacter[a] = newCharacter[b];
	                            newCharacter[b] = tukar;
	                        }
	                    }
	                    System.out.println(newCharacter[a]);
	                }
	                
	                //Arrays.sort(character);
	                /*for(int indeksStep=1; indeksStep<sumIndeksItem; indeksStep++){
	                    for(indeksItem=0; indeksItem<sumIndeksItem-indeksStep; indeksItem++){
	                        //if(character[indeksItem][choice]>character[indeksItem+1][choice]){}
	                        System.out.println(character[indeksItem][indeksCharacter]);
	                    }
	                }*/
	                
	                /*for(int i=0; i<sumIndeksItem; i++){
	                    System.out.println(character[i][choice]);
	                }*/

	                brSourceFile.close();
	            }
	            catch(ArrayIndexOutOfBoundsException e){

	            }
	            catch(FileNotFoundException e){
	                System.err.println("File "+sourceFile+" Tidak ditemukan");
	                ulang = false;
	            }
	            catch(IOException e){

	            }
	        }
	        while(ulang);
	    }
	}

